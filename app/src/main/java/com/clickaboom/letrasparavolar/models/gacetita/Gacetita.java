package com.clickaboom.letrasparavolar.models.gacetita;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by clickaboom on 7/1/17.
 */

public class Gacetita implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("0")
    @Expose
    public String _0;
    @SerializedName("autor")
    @Expose
    public String autor;
    @SerializedName("1")
    @Expose
    public String _1;
    @SerializedName("titulo")
    @Expose
    public String titulo;
    @SerializedName("2")
    @Expose
    public String _2;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("3")
    @Expose
    public String _3;
    @SerializedName("fecha")
    @Expose
    public String fecha;
    @SerializedName("4")
    @Expose
    public String _4;
    @SerializedName("pdf")
    @Expose
    public String epub;
    @SerializedName("5")
    @Expose
    public String _5;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("6")
    @Expose
    public String _6;
    @SerializedName("length")
    @Expose
    public String length;
    @SerializedName("7")
    @Expose
    public String _7;
    @SerializedName("contador")
    @Expose
    public String contador;
    @SerializedName("8")
    @Expose
    public String _8;
    @SerializedName("estatus")
    @Expose
    public String estatus;
    @SerializedName("9")
    @Expose
    public String _9;

}
