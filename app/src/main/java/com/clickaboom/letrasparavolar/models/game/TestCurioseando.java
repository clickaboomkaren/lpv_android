package com.clickaboom.letrasparavolar.models.game;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karen on 15/06/17.
 */

public class TestCurioseando implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("nombre")
    @Expose
    public String nombre;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
}
